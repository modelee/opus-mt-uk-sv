---
tags:
- translation
license: apache-2.0
---

### opus-mt-uk-sv

* source languages: uk
* target languages: sv
*  OPUS readme: [uk-sv](https://github.com/Helsinki-NLP/OPUS-MT-train/blob/master/models/uk-sv/README.md)

*  dataset: opus
* model: transformer-align
* pre-processing: normalization + SentencePiece
* download original weights: [opus-2020-01-16.zip](https://object.pouta.csc.fi/OPUS-MT-models/uk-sv/opus-2020-01-16.zip)
* test set translations: [opus-2020-01-16.test.txt](https://object.pouta.csc.fi/OPUS-MT-models/uk-sv/opus-2020-01-16.test.txt)
* test set scores: [opus-2020-01-16.eval.txt](https://object.pouta.csc.fi/OPUS-MT-models/uk-sv/opus-2020-01-16.eval.txt)

## Benchmarks

| testset               | BLEU  | chr-F |
|-----------------------|-------|-------|
| JW300.uk.sv 	| 27.8 	| 0.474 |

